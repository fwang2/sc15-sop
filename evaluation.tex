\subsection{Continuous Provisioning Evaluation}
\label{sec:evaluation}
Recall that we introduced two different ad hoc provisioning policies in Section
\ref{adhoc} (controller-first and enclosure-first).
We compare the performance of our continuous provisioning
model with the two ad hoc provisioning policies in terms of data availability and provisioning cost,
by using the tool introduced in Section \ref{simulator2}.
We again use the Spider I file system for the evaluation.

\subsubsection{Evaluation of Data Availability}

\begin{figure*}[!t]
\subfigure[Number of Data Unavailability]{\label{unava_vs_budget}
\includegraphics[width=0.3\textwidth]{./figures/unava_vs_budget.pdf}}\hfill
\subfigure[Amount of Unavailable Data]{\label{unava_data_vs_budget}
\includegraphics[width=0.3\textwidth]{./figures/unava_data_vs_budget.pdf}}\hfill
\subfigure[Average Unavailable Duration]{\label{unava_duration_vs_budget}
\includegraphics[width=0.3\textwidth]{./figures/unava_duration_vs_budget.pdf}}
\caption{Performance comparison between different provisioning policies}
\vspace{-0.25in}
\end{figure*}

In this section, we present the evaluation results of different provisioning policies
to demonstrate the effectiveness of our optimized provisioning policy in reducing
data unavailability. Note that besides the two ad hoc provisioning
policies we mentioned before, we also include the evaluation results of the scenario
when unlimited provisioning budget is provided, which gives the lower bound for the
data unavailability. Here, unlimited provisioning budget means every individual component
in the system can have a spare part on-site. For example, in Spider I there are 96
controllers, thus we can maintain 96 spare controllers in the spare pool if unlimited
budget were provided.

First, we present the results of the average number of data unavailability events
during the 5-year operation of 48 SSUs, using different provisioning policies
in Figure \ref{unava_vs_budget}. The results illustrate that at least one data
unavailability event will occur during 5 years if no provisioning policy is used
(no provisioning budget is provided). Further, the optimized provisioning policy can reduce
the data unavailability more significantly with increasing provisioning budget
when compared to the ad hoc policies.

Since one data unavailability event might cause multiple RAID groups to become unavailable
simultaneously, the volume of data that can become unavailable due to
even a single unavailability event can range in the tens of terabytes.
For the Spider I file system, each RAID level 6 group is composed of 10 1TB disks.
Figure \ref{unava_data_vs_budget} shows the
average amount of data that might become unavailable
under different provisioning policies during 5 years of operations.
We calculate this with the knowledge of how many RAID groups are affected by each
data unavailability event.
Similar to the results shown in Figure \ref{unava_vs_budget}, the optimized provisioning
policy can also reduce the amount of unavailable data significantly. For example,
with an annual spare provisioning budget of just \$480K, the optimized provisioning
policy can protect as much as 90TB from becoming unavailable during the 5-year operation
of the storage system.

Moreover, the optimized provisioning policy also decreases the duration of data
unavailability as shown in Figure~\ref{unava_duration_vs_budget}. For the same
\$480K annual provisioning budget, the optimized provisioning policy reduces the duration of
data unavailability for the 48 SSUs in aggregate by as much as 52\% (more than 20 hours) 
and 81\% (more than 80 hours) compared to the enclosure-first and controller-first 
provisioning policies.

\begin{finding}
Optimal provisioning increases data availability significantly.
In fact, as the provisioning budget
increases, it continues to perform even better, getting closer to the unlimited
budget policy.
\end{finding}


\subsubsection{Evaluation of Provisioning Cost}

This section presents an evaluation of the cost of different provisioning policies.
First, we illustrate the total provisioning cost during 5 years using different
provisioning policies, given different annual budgets, in Figure \ref{fig:total_prov_cost}.
Different from the two ad hoc policies, which try to squeeze every penny of the budget,
the cost of our optimized provisioning policy does not increase with the budget linearly.
The reason behind this is that the optimized provisioning policy allocates budget based on
an accurate failure estimation and failure dependency analysis, which are more
economical and efficient compared to the ad hoc policies.

\begin{figure}[ht]
    \label{fig:subfigures}
    \begin{tabular}{c}

            \includegraphics[scale=0.24]{./figures/total_prov_cost.pdf}

       \end{tabular}

%    \end{center}
\hrulefill  {\caption{Total provisioning cost in 5 years using different provisioning policies.\label{fig:total_prov_cost}}}
\vspace{-0.1in}
\end{figure}

Finally, we illustrate the cost for spare provisioning the 48 SSUs in each
year using the optimized provisioning policy, given
different annual budget limits (Figure \ref{fig:opt_annual_cost}).
Two interesting observations can be made from Figure \ref{fig:opt_annual_cost}.
First, the annual provisioning cost decreases year after year. This is because
many FRUs in Spider I have decreasing failure rates (see Figure \ref{cdf}).
Second, increasing the annual provisioning budget does not necessarily increase
the annual provisioning cost. For example, when the annual budget is increased
to \$480K, the provisioning cost is almost the same as when the annual budget is \$360K.
This is because the optimized provisioning policy attempts not to
over-provision the spare parts, i.e.,
no more spare parts will be added if what is available
is equal to what is expected to fail next year, resulting in cost savings.

%Second, the annual spare provisioning cost does not increase over time and hence, creates more oppurtunity 
%for cost savings. 
%For example, the spare provisioning cost is almost the same for both cases when the annual budget is 
%\$360\,K and \$480K. 
%This is because the optimized provisioning policy attempts not to over-provision the spare parts. That is, additional spare parts will not be added if currently available spare parts are equal to the number of components that are expected to fail next year. This in-turn results in creating more opportunity for cost savings.}


\begin{finding}

Optimal provisioning results in significant cost-savings.
%compared
%to other intuitive and first-guess policies such as Controller-first or
%Enclosure-first.
The resulting savings can be more than 10\% of the total
storage system cost over the operational life of a large-scale storage system.

\end{finding}
\vspace{-0.2in}

%\textbf{Devesh: check the correctness of above? \color{red} Lipeng: I think it's correct.}

%\textbf{Devesh: what about the insight related to the identifying and
%eliminating points of failure? \color{red} Lipeng: As shown in Figure
%\ref{unava_duration_vs_budget}, if we provision enough spare disk enclosures
%(enclosure-first policy was used), the data unavailable duration is also
%reduced considerably (though not as much as that reduced by our optimized
%provisioning) which further verifies that Spider I is more vulnerable to disk
%enclosure
%failures as we mentioned in Section \ref{adhoc}.}


\begin{figure}[ht]
    \label{fig:subfigures}
    \begin{tabular}{c}

            \includegraphics[scale=0.24]{./figures/opt_annual_cost.pdf}

       \end{tabular}

%    \end{center}
\hrulefill  {\caption{Annual cost for optimized provisioning policy.\label{fig:opt_annual_cost}}}
\vspace{-0.25in}
\end{figure}

%which consists 48 identical
%SSUs, and adopts RAID 6 configuration, as our evaluation case.
%
%\subsubsection{Provisioning only one type of FRU}
%
%%Before we present the evaluation results of our optimized provisioning policy,
%We first present the results of data unavailability and provisioning cost if
%spare provisioning is provided for only one type of FRU, e.g., the disk drives or
%controllers, during the entire operation time of the storage system, under an
%unlimited provisioning budget.  The is to see
%which type of FRU has the most impact on the data unavailability of the entire
%system.
%At the beginning of each year, the spare pool is checked, and if a specific
%type of FRU has no spare part, a new one is added to the pool.
%
%\begin{figure}[!t]
%\centering
%\includegraphics[width=3.4in]{./figures/provisiononefruunavai.pdf}
%\caption{Provisioning only one type of FRU}
%\vspace{-0.20in}
%\label{provisiononefruunavai}
%\end{figure}
%
%Figure \ref{provisiononefruunavai} shows the average
%number of data unavailability events during the 10-year operation of 48 SSUs,
%if we only provide spare provisioning for one type of FRU. Additionally, we
%also illustrate the 10-year cost for only provisioning one type of FRU in
%Figure \ref{provisiononefrucost}.
%
%\begin{figure}[!h]
%\centering
%\includegraphics[width=3.4in]{./figures/provisiononefrucost.pdf}
%\caption{10-year Cost for provisioning only one type of FRU}
%\vspace{-0.15in}
%\label{provisiononefrucost}
%\end{figure}
%
%We can see that although the cost for provisioning each type of
%FRU is significantly different, the results on data unavailability are similar.
%This means that only provisioning one type of FRU during the entire operation time of the
%storage system is not
%effective for reducing data unavailability.
%There is the need for an optimized provisioning policy
%that is able to reconcile reliability characteristics of all FRUs and the dependencies
%among them.
%
%\begin{figure*}[!t]
%\subfigure[Number of Data Unavailability]{\label{optimizedprovisioning}
%\includegraphics[width=0.3\textwidth]{./figures/provisioning.pdf}}\hfill
%\subfigure[Amount of Unavailable Data]{\label{prov_volume_data_unavai}
%\includegraphics[width=0.3\textwidth]{./figures/prov_volume_data_unavai.pdf}}\hfill
%\subfigure[Average Unavailable Duration]{\label{prov_unava_duration}
%\includegraphics[width=0.3\textwidth]{./figures/prov_unava_duration.pdf}}
%\caption{Performance comparison between different provisioning policies}
%\vspace{-0.15in}
%\end{figure*}
%
%
%\subsubsection{Optimized Provisioning vs. Other Provisioning Policies}
%
%\begin{figure}[!b]
%\centering
%\includegraphics[width=3.0in]{./figures/provisioningcost.pdf}
%\caption{Cost for optimized provisioning for each year}
%\vspace{-0.25in}
%\label{provisioningcost}
%\end{figure}
%
%
%Recall that we introduced two different methods for optimizing the provisioning
%policies (Section \ref{sec:approach}).
%%, we proposed two different methods for optimizing provisioning policies.
%{\em Method 1} counts the unavailable paths of each triple-disk combination in a
%RAID group, while {\em method 2} counts all unavailable paths caused by each FRU
%failure in a RAID group.
%%For both of these two optimized provisioning policies
%%(\emph{method 1} and \emph{method 2}),
%We compare these two optimized provisioning techniques against
%\emph{no provisioning}, \emph{random provisioning} and \emph{provisioning
%with unlimited budget}. \emph{No provisioning} means that there is no
%budget for spare provisioning, and once there is an FRU failure, we have
%to wait for the part to be delivered to the field. \emph{Random provisioning}
%means that at the beginning of each year, we provide spare parts for FRUs that
%are randomly selected from those that have no part in the spare pool, under a
%limited budget. \emph{Provisioning with unlimited budget} means that at the
%beginning of each year, if an FRU has no part in the spare pool, we add one no
%matter how much it costs.  \emph{No provisioning} policy gives the upper bound
%of data unavailability, while \emph{provisioning with unlimited budget} gives
%the lower bound.  Figure \ref{optimizedprovisioning} shows the results of the
%average number of data unavailability events during the 10-year operation of 48
%SSUs. We can see that the
%optimized provisioning policies have lower data unavailability events than the
%random provisioning policy, and \emph{method 2} achieves best performance.
%%\begin{figure}[!t]
%%\centering
%%\includegraphics[width=3.4in]{./figures/provisioning.pdf}
%%\caption{Average Number of Data Unavailability Events}
%%\vspace{-0.10in}
%%\label{optimizedprovisioning}
%%\end{figure}
%While it may appear that optimized provisioning is only better than random provisioning
%by a few failure events, it is worth noting that the volume of data that can become unavailable due to
%even a single event can range in the tens of terabytes.
%For the Spider I file system, each RAID level 6 group is composed of 10 1TB disks,
%and the average number of data unavailability events in all RAID groups
%allows us to estimate the amount of data that might become unavailable
%during the operation of the system (Figure~\ref{prov_volume_data_unavai}).
%For example, with an annual provisioning
%budget of just under \$1M, optimized provisioning (method 2) can protect as much as 30TB
%from becoming unavailable during the entire operations of Spider I.
%Further,
%%to reducing the average number of data unavailability events and the amount of data that
%%might become unavailable,
%optimized provisioning also decreases the duration of data
%unavailability as shown in Figure~\ref{prov_unava_duration}.
%%plots the total unavailable duration of the 48 SSUs in Spider I
%%during a 10-year operation simulation for the different provisioning policies.
%For the same \$1M annual provisioning budget, optimized provisioning reduces the duration of
%data unavailability for the SSUs in aggregate by 500 hours compared to random provisioning.
%%in Fig. \ref{prov_unava_duration}. The results also demonstrate
%%our optimized provisioning policies can achieve lower unavailable duration.
%%\begin{figure}[!t]
%%\centering
%%\includegraphics[width=3.4in]{./figures/prov_volume_data_unavai.pdf}
%%\caption{Average Amount of Unavailable Data}
%%\vspace{-0.10in}
%%\label{prov_volume_data_unavai}
%%\end{figure}
%%\begin{figure*}[!ht]
%%\minipage{0.3\textwidth}
%%  \includegraphics[width=\linewidth]{./figures/provisioning.pdf}
%%  \caption{Number of Data Unavailability}\label{optimizedprovisioning}
%%\endminipage\hfill
%%\minipage{0.3\textwidth}
%%  \includegraphics[width=\linewidth]{./figures/prov_volume_data_unavai.pdf}
%%  \caption{Amount of Unavailable Data}\label{prov_volume_data_unavai}
%%\endminipage\hfill
%%\minipage{0.3\textwidth}
%%  \includegraphics[width=\linewidth]{./figures/prov_unava_duration.pdf}
%%  \caption{Average Unavailable Duration}\label{prov_unava_duration}
%%\endminipage
%%\caption{Performance Comparison between Different Provisioning Policies}
%%\vspace{-0.15in}
%%\end{figure*}
%
%\begin{comment}
%\begin{figure*}[!t]
%\subfigure[Number of Data Unavailability]{\label{optimizedprovisioning}
%\includegraphics[width=0.3\textwidth]{./figures/provisioning.pdf}}\hfill
%\subfigure[Amount of Unavailable Data]{\label{prov_volume_data_unavai}
%\includegraphics[width=0.3\textwidth]{./figures/prov_volume_data_unavai.pdf}}\hfill
%\subfigure[Average Unavailable Duration]{\label{prov_unava_duration}
%\includegraphics[width=0.3\textwidth]{./figures/prov_unava_duration.pdf}}
%\caption{Performance comparison between different provisioning policies}
%\vspace{-0.15in}
%\end{figure*}
%\end{comment}
%
%Next we evaluate the cost of the different provisioning policies.  We note that
%the 10-year provisioning cost of the various provisioning policies
%%(\emph{method 1} and \emph{method 2}), \emph{random provisioning} and
%%\emph{provisioning with unlimited budget}
%are roughly the same (around \$11M).
%%\$11,568,000, \$11,551,000,\$11,382,000,\$11,586,000 (apparently, there is no
%%provisioning cost for \emph{No provisioning} policy). Here, for \emph{method
%%1}, \emph{method 2} and \emph{random provisioning}, we use \$3,840,000 as the
%%annual provisioning budget (\$80,000 per SSU per year).  \begin{figure}[!h]
%%\centering \includegraphics[width=3.4in]{./figures/provisioningtotalcost.pdf}
%%\caption{Cost for Different Provisioning Policies in 10 Years}
%%\vspace{-0.10in} \label{provisioningtotalcost} \end{figure}
%Since the number of FRU failures during the lifetime of a large-scale storage
%system is determined by the reliability characteristics of the FRUs, no matter
%which provisioning policy is used, the number of FRUs that need to be replaced
%during same time are similar.
%%That's why we can observe that during a relatively long operation time (10
%%years), the provisioning cost produced by every provisioning policy are
%%approximately equal to each other from the evaluation results.
%
%%Finally, we illustrate the cost for the spare provisioning of 48 SSUs in each
%%year by using the optimized provisioning policy (\emph{method 2}), given
%%different annual budget limits (Figure \ref{provisioningcost}).
%
%Finally, we illustrate the cost for the spare provisioning of 48 SSUs in each
%year by using the optimized provisioning policy (\emph{method 2}), given
%different annual budget limits (Figure \ref{provisioningcost}).
%The figure shows that if the annual budget is high, the provisioning cost in
%the first few years are much higher than the later years.  Interestingly, no
%matter what the annual budget, the total cost for 10-year provisioning is
%similar.  This suggests that we can increase the annual budget in the first few
%years while decreasing it in the later years if we have a fixed total budget.
%This is because if we provide more spare parts in the first few years, the data
%unavailability as well as the provisioning cost in following years will
%decrease dramatically.


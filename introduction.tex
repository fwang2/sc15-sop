\section{Introduction}
\label{sec:introduction}

Provisioning an extreme-scale storage system needs to factor in a variety of
goals such as capacity, performance and availability, while adhering to a
fixed price point.  While each individual target is important, what is even
more critical is how they are reconciled with each other towards a practical
solution. Some examples of extreme-scale deployments include the Oak Ridge Leadership
Computing Facility's (OLCF) Spider I and II storage systems, Livermore
Computing Center's Sequoia storage system~\cite{sequoia} and Riken  Advanced
Institute for Computational Science's K-Computer storage system~\cite{riken}.
Let us consider OLCF's Spider I and II Lustre-based parallel file systems, which
were among the world's fastest Lustre storage systems at the time of their
deployments, and intended for the Jaguar (No. 1 on the June 2010 Top500
list~\cite{top500}) and Titan (No. 2 on the current Top500 list~\cite{top500})
supercomputers, respectively.
%They were designed and deployed to serve all OLCF resources but Spider
%I~\cite{spider1} mainly served Jaguar (No. 1 supercomputer in June 2010 Top500
%list~\cite{top500}) and Spider II~\cite{Spider2} serves Titan (No. 2
%supercomputer in the current Top500 list~\cite{top500}), respectively.  and
%serves the Titan supercomputer (No. 2 in the Top500 list).
Spider I offered 10 PB of capacity, using 13,440 1 TB drives, organized in RAID 6 arrays,
delivering 240 GB/s; Spider II offers 40 PB of capacity, using 20,160 2 TB
drives, using RAID 6, delivering 1 TB/s aggregate throughput.
%The Spider file
%systems comprised of tens of thousands of disks, and provided hundreds of
%gigabytes per second I/O performance.
%Each of these extreme-scale deployments had tens of thousands of disks and
%peripheral components.
It is seldom the case that such configurations are
readily arrived at by either the customer or the vendor.  In fact, storage
system designers negotiate such terms with vendors during the procurement
process, based on broad targets derived from user requirements and a desire to
provide a certain quality of service to users.  Provisioning is therefore a
complex reconciliation process between often competing goals.

\begin{comment}
Large-scale storage systems are often built with \emph{scalable system units}
(SSU). These basic building blocks are replicated to meet the required
capacity, performance and availability targets. From an initial storage
system provisioning and deployment standpoint, designers will need to consider
questions such as the following. How many SSUs will achieve the desired capacity and
performance under a fixed price envelope?  What sized drives will each SSU
host?  What is the impact on cost, performance, and footprint assuming a
smaller sized drive can achieve the desired capacity?
%In other words, how many more SSUs are needed to fulfill the capacity
%requirement?
How will the drive size impact availability and rebuild times of the RAID
group, and consequently, the performance of the RAID array? What are the
redundant data paths within an SSU to ensure data availability? From a
day-to-day operations standpoint, designers need to answer questions such as
the following.  What are the expected failure rates of the storage subsystem
components, and how do they impact data availability?  Can we take advantage
of the above fact to better provision spares in order to improve data
availability, rather than blindly allocating funds?
\end{comment}

Large-scale storage systems are often built with \emph{scalable system units}
(SSU). These basic building blocks are replicated to meet the required
capacity, performance and availability targets. Oftentimes, during the
initial provisioning stage, system designers need to optimize the
configuration under a fixed budget. These configuration options include (but
not limited to) optimizing the number of SSUs for capacity and performance;
selecting a drive type and/or size while balancing capacity, performance and
rebuild times; eliminating single points of failures, and increasing system
redundancy.

From a day-to-day operations view point, the focus shifts to maintaining
system health and minimizing data unavailability and loss incidents. One
effective way to increase both system and data availability is to reduce the
time spent in repairing and replacing failed components.  This can be achieved
by having an on-site spare parts pool. However, uniformly provisioning spare
parts for all hardware components is neither cost effective nor practical.
Therefore, a critical concern in extreme-scale storage system deployment and
management is designing and implementing an effective spare provisioning
policy.

These are but a few design and operational constraints to be considered.
However, system designers are left with back of the envelope calculations and
rules of thumb when it comes to such provisioning decisions.  There are no
models, simulations or tools that designers can use to plug in parameters, and
answer such {\em what-if} scenarios.


{\bf Contributions:} In this paper, we address these provisioning challenges that storage
system designers face by building tools that can help
reconcile key figures of merit, and answer {\em what-if} scenarios.  
Our study and the tools are 
primarily intended for storage system architects, administrators 
and procurement teams, for their provisioning needs.
We present a
quantitative analysis on provisioning a large-scale HPC storage system. Our results shed light on
%What
the following: the components that affect cost, performance and capacity; the components that are
most important in building a performance-oriented storage system; and the effect
%How does
redundancy among components has on the reliability of the overall system and
its cost-effectiveness.
%We further share interesting insights about building a
%large-scale HPC storage system.
In particular, we show that the I/O controller
and disk enclosures play a more dominant role than disk drives in building a
high-performance, cost-effective HPC storage system. We also show that it is
more cost-efficient to saturate the I/O controllers of one SSU before scaling
out. Scaling up SSUs without saturating the controllers may potentially save
disk drive costs, but increases the overall cost significantly while decreasing
the system reliability. In order to ensure a highly available
operational experience, we also build a dynamic optimization model for
continuous spare provisioning. Our results demonstrate that the
model is not only able to minimize the number of unavailability events,
but also reduce the window of data unavailability. 


\subsection{Field-gathered Failure Data Analysis}
\label{analysis}

%A large-scale storage system is often complex and comprises of tens of
%thousands of disk drives and other hardware components, such as power supplies,
%RAID controllers, etc.

In this section, we analyze the field gathered failure data of different
hardware components used in the Spider I file system at OLCF, and provide an
account of the key findings learned through the process.

\subsubsection{Vendor Provided Reliability Metrics}

System vendors often provide AFR (annual failure rate) or MTTF (mean time to
failure) of each type of FRU (field replaceable unit).
As stated earlier, Spider I consists of 48 SSUs,
and the AFRs of the FRUs are listed in Table \ref{frus}.
Vendor provided reliability metrics can be used to derive a
coarse-grained estimation of a storage subsystem's reliability. As an example, one
model that has been widely used to estimate the data availability of
disk redundancy groups is continuous Markov chain, which has an
underlying assumption that the failure rates of disk drives are constant (time
independent) \cite{Gibson1993, Chen1994, Schulze1989, Patterson1988}. With such a
model, the vendor-provided metrics, AFRs and MTTF, can be used to establish
the failure model of each disk drive, which assumes that the time to failure of
disk drives is an exponential distribution.

\begin{table}[!t]
\centering
\footnotesize
\scalebox{0.7}{
%\begin{tabular}{|c||c||c||c||c|}
\begin{tabular}{|p{3.0cm}|p{0.2cm}|p{1.0cm}|p{0.4cm}|p{0.8cm}|p{0.6cm}|}
\multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{Unit} & \multicolumn{1}{c}{Vendor} &\multicolumn{1}{c}{Actual}\\
\multicolumn{1}{c}{} & \multicolumn{1}{c}{Number} & \multicolumn{1}{c}{IDs} & \multicolumn{1}{c}{Cost (\$)} & \multicolumn{1}{c}{AFR} &\multicolumn{1}{c}{AFR}\\
\cline{1-6}
Controller  & 2      & 15-16  &  10,000& 4.64\% &16.25\%\\
\hline
House Power Supply (Controller) & 2  & 1-2 &2,000& 0.83\% &4.38\%\\
\hline
Disk Enclosure & 5 & 27-31 & 15,000&0.23\% &1.17\%\\
\hline
House Power Supply (Disk Enclosure) & 5 & 3-7 & 2,000 & 0.08\% &8.50\%\\
\hline
UPS Power Supply & 7 & 8-14 &1,000 &3.85\% &NA\\
\hline
I/O Module & 10 & 17-26 &1,500 &0.38\% &0.92\%\\
\hline
Disk Expansion Module (DEM) & 40 & 32-71& 500&0.23\% &0.29\%\\
\hline
Baseboard & 20 & 72-91 & 800 &0.23\% &NA\\
\hline
Disk Drive & 280 & 92-371& 100&0.88\%  &0.39\%\\
\hline
\end{tabular}
}
\caption{FRUs in one scalable storage unit}
\vspace{-0.2in}
\label{frus}
\end{table}

%Besides the AFR information, we also obtain the MTTR (mean time to repair) of
%different FRUs used in Spider I file system from system administrators.
%According to their logs, most of the failed FRUs can be repaired or replaced
%within 24 hours.
\subsubsection{Field Failure Data}




\begin{figure*}[!t]
\subfigure[Controllers]{\label{ctrlcdf}
\includegraphics[width=0.3\textwidth]{./figures/ctrlcdf.pdf}}\hfill
\subfigure[Dems (Disk Expansion Modules)]{\label{demcdf}
\includegraphics[width=0.3\textwidth]{./figures/demcdf.pdf}}\hfill
\subfigure[Disk Enclosures]{\label{diskenclcdf}
\includegraphics[width=0.3\textwidth]{./figures/diskenclcdf.pdf}}\hfill \\
\subfigure[Disk Drives]{\label{diskcdf}
\includegraphics[width=0.3\textwidth]{./figures/diskcdf.pdf}}\hfill
\subfigure[House Power Supply]{\label{powcdf}
\includegraphics[width=0.3\textwidth]{./figures/powcdf.pdf}}\hfill
\subfigure[I/O Modules]{\label{iomodcdf}
\includegraphics[width=0.3\textwidth]{./figures/iomodcdf.pdf}}
\caption{Distribution of time between device replacements for different types of FRUs in Spider I}
\vspace{-0.25in}
\label{cdf}
\end{figure*}

Besides the vendor-provided metrics, system administrators  typically maintain
field-gathered failure and replacement data. Such information is much closer
to the reality than vendor provided reliability metrics.  In fact, by
analyzing the field-gathered failure data of storage systems, several existing
studies have shown that the failure rates of disk drives and other
hardware components can vary over time \cite{ssrctr-09-08, Pinheiro2007}.
%This means that assumptions in existing failure models of disk subsystems
%might not be accurate.

The failure and replacement data for Spider I was collected from all of the 48 SSUs
during its 5-year operational period.  The dataset contains timestamps when
%requirements for
device replacement was needed.
%were reported.
We first count the number
of failures of each type of FRU during 5 years, and then calculate
their actual AFRs. The results are summarized in Table \ref{frus}. Below is a
list of the key findings:

\begin{finding}
 The actual annual failure rate (AFR) of Spider I disks is only
    0.39\% -- much smaller than what has been reported in previous
    studies~\cite{Schroeder2007}. It is hard to generalize this as the
    environment, testing conditions and vendors are quite different.
    Efficient facilities support, e.g., better power and cooling infrastructure,
    might be a factor here. However, it is not possible to quantitatively establish
    a causal relationship between operating conditions and disk drive failure rate.
\end{finding}

\begin{finding}

    Aggressive burn-out tests at the time of system deployment help eliminate
    potential problematic or slower disks early on, which improves the overall aggregate
    parallel performance. %by eliminating slower disks.
	It also keeps the disk
    AFR low by removing potential problematic disks from the population.

\end{finding}

On the point of stress testing and slow disk identification, there are no
community standards for this process. Our method involved
individually stressing each SSU, and identifying the slowest disk RAID groups.
Then, we exercised those groups separately, and collected latency statistics on the
disks individually. This process should be performed during initial deployment,
and repeated periodically to keep a healthy and uniformly performing disk
population.
Our records indicate that the AFR before the acceptance of
the Spider I system was much higher (2.2\%).
Our early testing helped remove close to 200 slow or bad disks.
This resulted in a much lower AFR during production (0.39\%).

\begin{finding} Non-disk components of Spider I have
    higher AFRs than vendor provided metrics.

\end{finding}

While this comes as a surprise,
%as we had expected the reverse to be true.
it also
suggests that future studies should carefully model and account for the
reliability of non-disk components as they contribute heavily towards the
overall reliability of the system.

%    \textbf{Devesh: Any study that has found this before? Is this novel?
%    \color{red} Lipeng: In paper ``Are Disks the Dominant Contributor for
%Storage Failures?'' (W. Jiang, FAST'08), the authors also found that non-disk
%components account for significant percentages of failures, but they didn't
%present the vendor-provided metrics and compare them with the measured AFRs.}

%saying "I don't know twice back to back" shows we are shallow in our
%analysis. These non-disk components were manufactured by different vendors,
%so this deviation more likely points to an environmental problem at OLCF.

Next, we derive the empirical, cumulative distribution function (CDF) of
the time between device replacements for different types of FRUs (Figure \ref{cdf}).
We use four distributions to fit the CDF (Figure \ref{cdf}).

\begin{comment}
\begin{finding}
    Disk drive failures can be more accurately modeled by joining
    two different distributions.
\end{finding}
\end{comment}

For example, as shown in Figure \ref{diskcdf}, when the time between disk
replacements is relatively small, a Weibull distribution with
decreasing failure rate is a better fit; with increasing time between disk replacements,
the failure rate is stable, and an exponential distribution
is a better fit. This observation indicates that in reality the
failure rate of disk drives could be neither constant nor monotonically
increasing or decreasing, which differs from what is usually assumed by many
existing studies \cite{SchwarzXMLHN04, Elerath2007,ssrctr-09-08, Elerath2014}.

\begin{finding}
    Disk drive failures can be more accurately modeled by joining
    two different distributions.
\end{finding}

%\textbf{Devesh: so what is different here than usual bathtub curve theory?
%\color{red} Lipeng: If I remember correctly, in the bathtub curve theory,
%time to failure of each individual device within a large device population is
%used to calculate the failure rate. That's why there are so called ``infant
%mortality'', ``normal life'' and ``end of life wear-out''. In our failure
%data analysis, we use the time between failures of different devices to
%calculate the failure rate of devices belong to same type, I think it's
%differs from the bathtub curve theory.}
%We the number of FRU failures in fixed time intervals, and
%used polynomial regression \cite{Montgomery2012} to estimate the hazard
%functions to fit the field-gathered failure data.
%\begin{figure*}[!ht]
%\minipage{0.3\textwidth}
%  \includegraphics[width=\linewidth]{./figures/ctrlhzfunc.pdf}
%  \caption{Failure rate of controllers}\label{ctrlhzfunc}
%\endminipage\hfill
%\minipage{0.3\textwidth}
%  \includegraphics[width=\linewidth]{./figures/powhzfunc.pdf}
%  \caption{Failure rate of power supplies}\label{powhzfunc}
%\endminipage\hfill
%\minipage{0.3\textwidth}
%  \includegraphics[width=\linewidth]{./figures/demhzfunc.pdf}
%  \caption{Failure rate of DEMs}\label{demhzfunc}
%\endminipage
%\vspace{-0.15in}
%\end{figure*}
%\begin{figure*}[!ht]
%\minipage{0.3\textwidth}
%  \includegraphics[width=\linewidth]{./figures/diskenclhzfunc.pdf}
%  \caption{Failure rate of disk enclosures}\label{diskenclhzfunc}
%\endminipage\hfill
%\minipage{0.3\textwidth}
%  \includegraphics[width=\linewidth]{./figures/iomodhzfunc.pdf}
%  \caption{Failure rate of I/O modules}\label{iomodhzfunc}
%\endminipage\hfill
%\minipage{0.3\textwidth}
%  \includegraphics[width=\linewidth]{./figures/diskhzfunc.pdf}
%  \caption{Failure rate of disks}\label{diskhzfunc}
%\endminipage
%\vspace{-0.15in}
%\end{figure*}

%Figure \ref{ctrlhzfunc} to Figure \ref{diskhzfunc} illustrate how FRU failure rates
%vary over time. For example, Figure \ref{ctrlhzfunc} represents the empirical
%failure rates obtained from the field-gathered failure data for RAID
%controllers, and is calculated as follows: $\text{failure rate} = \frac{\text{\#
%of controller failures in $\Delta t$}}{\text{\# of controllers in system}\times
%\Delta t}$, where $\Delta t$ is the fixed time interval (bars' width shown in
%Fig. \ref{ctrlhzfunc}) in which we accumulate the number of controller
%failures.
%%With knowing the empirical values of failure rate,
%We use a third degree polynomial regression function to estimate the parameters
%of the hazard function to fit the failure data.
%%The degree of polynomial regression function we choose is 3 and the fitted
%%hazard function of controllers is illustrated by the curve in Fig.
%%\ref{ctrlhzfunc}.
%From the figures, we make two observations. First, the failure rates of some types
%of FRUs are non-constant. For example, as shown in Figure \ref{diskcdf}, when time between
%disk replacements is relatively small, it fits a Weibull distribution with
%decreasing failure rate better; with the time between disk replacements increasing,
%the failure rate becomes stable and an exponential distribution fits the empirical data
%better. Second, all FRUs except the disk drives have higher failure rates
%than those provided by the manufacturers. For example, the vendor-provided AFR
%of controller is 4.64\%, while the AFR calculated from device replacement data
%is 16.69\%. This might suggest that the reliability of those non-disk components in extreme-scale
%storage systems has been underestimated, since most existing research
%concentrate on the reliability of the disk drives.  However, in reality,
%non-disk components could fail more often than we expect.

FRU replacement times for Spider I have not been recorded or
shared with the public. However, it was stated that most of these replacements were
completed within 24 hours, if spare parts were available on-site. If there were
no spare parts on-site, a replacement was awaited, and usually took at least 7
days~\cite{repair}.

\begin{comment}
In order to analyze the distribution
of time to failure of different types of FRU, we select 17 different kinds of parametric
probability distributions and fit them to the field-gathered failure data of each type of FRU one by one.
The 17 probability distributions we use are:
\emph{Beta}, \emph{Birnbaum-Saunders}, \emph{Exponential}, \emph{Extreme value}, \emph{Gamma},
\emph{Generalized extreme value}, \emph{Generalized Pareto},
\emph{Inverse Gaussian}, \emph{Logistic}, \emph{Log-logistic}, \emph{Lognormal}, \emph{Nakagami}, \emph{Normal}, \emph{Rayleigh}, \emph{Rician}, \emph{t Location-Scale}, and \emph{Weibull}.

\begin{figure*}[!ht]
\minipage{0.3\textwidth}
  \includegraphics[width=\linewidth]{./figures/controllerpdf.pdf}
  \caption{PDF of Controller Failures}\label{controllerpdf}
\endminipage\hfill
\minipage{0.3\textwidth}
  \includegraphics[width=\linewidth]{./figures/powpdf.pdf}
  \caption{PDF of Power Supply Failures}\label{powpdf}
\endminipage\hfill
\minipage{0.3\textwidth}
  \includegraphics[width=\linewidth]{./figures/dempdf.pdf}
  \caption{PDF of DEM Failures}\label{dempdf}
\endminipage
\vspace{-0.15in}
\end{figure*}
\begin{figure*}[!ht]
\minipage{0.3\textwidth}
  \includegraphics[width=\linewidth]{./figures/diskenclpdf.pdf}
  \caption{PDF of Disk Enclosure Failures}\label{diskenclpdf}
\endminipage\hfill
\minipage{0.3\textwidth}
  \includegraphics[width=\linewidth]{./figures/iomodpdf.pdf}
  \caption{PDF of I/O Module Failures}\label{iomodpdf}
\endminipage\hfill
\minipage{0.3\textwidth}
  \includegraphics[width=\linewidth]{./figures/diskpdf.pdf}
  \caption{PDF of Disk Failures}\label{diskpdf}
\endminipage
\vspace{-0.15in}
\end{figure*}

Based on this data and our understanding of it, we then sort the fitting
results for FRUs and draw probability density functions of 4 most fitted
distributions, as well as the one calculated from real data.  The density
functions for each type of FRU are shown in Fig.
\ref{controllerpdf}-\ref{diskpdf}, respectively.  For example, as shown in Fig.
\ref{controllerpdf}, the bars represent the empirical density function
calculated from the field-gathered controller failure data. The area of each
bar equals to the frequency of controller failures occurred within that time
interval. The 4 different curves represent 4 most fitted parametric
distributions, which are sorted by Bayesian information criterion
\cite{missing-citation-04} in descending order.

\end{comment}

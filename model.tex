\subsection{Dynamic Spare Provisioning Model}
\label{sec:model}

Our proposed  model aims to optimize the spare provisioning policy
for large-scale storage systems in order to achieve high data availability,
given a limited provisioning budget. The notations of all the
symbols used in this section are listed in Table~\ref{tablenotes}.

\begin{table}[t]
\centering
\scalebox{0.7}{
\begin{tabular}{|l|l|}
 \hline
 $N$ & Number of types of FRU in system\\
 \hline
 $\emph{FRU}_i$ & $i$-th type of FRU \\
 \hline
 $f_{i}(x)$ & PDF of time between failures of $\emph{FRU}_i$ \\
 \hline
 $F_{i}(x)$ &CDF of time between failures of $\emph{FRU}_i$ \\
 \hline
 $h_{i}(x)$ & Hazard rate of $\emph{FRU}_i$ \\
 \hline
 $\emph{MTBF}_i$ & Mean time between failures of $\emph{FRU}_i$ \\
 \hline
 $\emph{MTTR}_i$ & Mean time to repair of $\emph{FRU}_i$ \\
 \hline
 $\tau_i$ & Delay caused by waiting for a new $\emph{FRU}_i$ to be delivered\\
 \hline
 $t_{i}^{fail}$ & Time point when last failure of $\emph{FRU}_i$ occurred \\
 \hline
 $t^{cur}$ & Current time when we need to update the spare pool\\
 \hline
 $t^{next}$ & Next time when we need to update the spare pool\\
 \hline
 $m_i$ & Impact $\emph{FRU}_i$ has on data unavailability \\
 \hline
 $b_i$ & Unit price of $\emph{FRU}_i$ \\
 \hline
 $B$ & Annual budget for spare provisioning \\
 \hline
\end{tabular}
}
%\normalsize
\caption{Notations of Symbols}
\vspace{-0.25in}
\label{tablenotes}
\end{table}

\subsubsection{Intuition and Assumption}

The impact each FRU has on system availability is usually determined by two factors:
its own reliability (e.g., some FRUs fail less often than others,
or can be repaired more quickly) and the system architecture (e.g.,
in Spider I, as the lack of hardware redundancy makes the system more
vulnerable to disk enclosure failures, disk enclosures have more
impact on data availability). However, few existing ad hoc provisioning policies focus
on these two factors simultaneously. Therefore, the basic idea behind our
spare provisioning optimization model is to quantify both of these factors,
and allocate more budget towards provisioning spare parts for FRUs
that have more impact on the data availability of the storage system.

The assumption we made about the provisioning budget is simple but realistic.
Specifically, at the beginning of each year, system administrators get a fixed
budget that we call the annual budget, and use it to prepare spares for different
FRUs according to a specific provisioning policy.

%\subsubsection{Intuition and Assumption}
%\begin{comment}
%A large-scale storage system is often complex and comprises of tens of thousands of
%disk drives and other hardware components, such as power supplies, RAID
%controllers, etc. Some of these components may consist of even smaller
%subcomponents. For example, a disk drive consists of spindle motor, and actuators.
%It is neither feasible nor necessary to model every single hardware component
%inside a disk drive or a RAID controller. Instead, we assume FRUs (field replaceable units)
%as the building blocks of the storage systems in our model, since information
%such as field AFR (annual failure rate) of each FRU are more or less maintained
%by both system vendors and HPC operators alike. With such information,
%we can establish reasonable failure models for each FRU,
%which will further drive the reliability estimate at the system level.
%\end{comment}
%\begin{comment}
%Given the reliability information, e.g., AFR of each FRU
%either from system vendors or HPC operators, we can establish a reasonable failure
%model for each FRU, which will further drive the reliability estimate at the
%system level.
%Ideally, given unbound budget, we can provide spare parts for each FRU to
%maximize the data availability of the storage system. However, in practice, the
%budget for purchasing and maintaining spare pool is always limited.
%Therefore, the basic idea of our spare provisioning optimization model
%is straightforward in that it allocates more budget towards provisioning spare parts for FRUs
%that are more critical to the data availability of the storage system.
%
%Due to the huge number of FRUs and their complex failure dependencies,
%it can be extremely difficult to
%derive the impact of each FRU on the data availability of the entire system.
%However, the importance of an FRU is usually determined by two
%factors: its own reliability (e.g., some FRUs are more
%reliable than others, or can be repaired more quickly), and the impact it has
%on other FRUs in the system (e.g, the failure of a disk
%enclosure might cause many other FRUs to become unavailable
%simultaneously). Therefore, we can quantify these two factors, and
%combine them together to represent how important each FRU is to the
%availability of the system.
%\end{comment}

\subsubsection{Reliability Characteristics of Different FRUs}

Since we already have the field-gathered failure data and vendor-provided AFR, quantifying
the reliability of each type of FRU is easy.
We obtain the probability density function (PDF) of the time between the failures of a type of FRU
by fitting the failure data.
Thereafter, we can estimate the number of failures of such a type of FRU that
will occur during a future period.
For example, we use $f_{i}(x)$ to
denote the PDF of the time between failures of $\emph{FRU}_i$, then the CDF can be calculated
as $F_{i}(x)=\int_{0}^{x}f_{i}(t)dt$. Based on this definition, the hazard rate of $\emph{FRU}_i$
is given as:
\begin{equation}
h_{i}(x)=\frac{f_{i}(x)}{1-F_{i}(x)}
\end{equation}
Let us assume the last failure of $\emph{FRU}_i$ occurred at time $t_{i}^{fail}$.
We need to estimate the number of failures, $y_i$, of $\emph{FRU}_i$ between the current time, $t^{cur}$,
when the spare pool is being updated and the next time, $t^{next}$, the spare pool will need an update.
This is given as follows:
\begin{equation}
\label{exp_num_fail}
y_i = \int_{t^{cur}-t_{i}^{fail}}^{t^{next}-t_{i}^{fail}}h_{i}(x)dx
\end{equation}
The above formula can estimate the expected number of failures between $t^{cur}$ and $t^{next}$
accurately if the time between the failures fits an exponential distribution, which has a
time-independent hazard rate. However, for a Weibull distribution, if the time between
updating the spare pool is relatively longer compared to the mean time between failures,
this formula cannot give an accurate estimation.
This is because, once a failure
occurs between $t^{cur}$ and $t^{next}$, which is very possible because of the short
mean time between failures, the hazard rate should increase. Therefore, for a Weibull distribution,
if
\begin{equation}
\frac{t^{next}-t^{cur}}{\emph{MTBF}_i} >\int_{t^{cur}-t_{i}^{fail}}^{t^{next}-t_{i}^{fail}}h_{i}(x)dx \text{,}
\end{equation}
we can use
\begin{equation}
y_{i} = \frac{t^{next}-t^{cur}}{\emph{MTBF}_i} \text{,}
\end{equation}
instead of (\ref{exp_num_fail}), where $\emph{MTBF}_i$ is the mean time between failure of
$\emph{FRU}_i$.
Given the PDF of the time between failures of $\emph{FRU}_i$, $f_{i}(x)$, $\emph{MTBF}_i$ can be
calculated by solving $\int_{0}^{\infty}xf_{i}(x)dx$.

In Spider I, if no spare part was available on-site, a device replacement will be
delayed by at least 7 days. If we use $\emph{MTTR}_i$ to denote the mean time to repair
of $\emph{FRU}_i$ when spare parts are available on-site, $\tau_i$ to denote the delay caused
by waiting for a new $\emph{FRU}_i$ to be delivered, then the total time spent on replacing
$\emph{FRU}_i$ is $\emph{MTTR}_i+\tau_i$, if there is no spare on-site when the replacement
is required.
%The reliability characteristics of each FRU can be quantified by the probability
%that the FRU is down at a specific time point. Note that this probability is
%different from the probability that the FRU fails at that time point because
%there is always some downtime for the failed FRU to be repaired or replaced. In
%fact, this probability can also be interpreted as the probability that the specific
%time point is within the FRU's downtime interval.
%
%\begin{figure}[!t]
%\centering
%\includegraphics[width=3in]{./figures/failurerepair.pdf}
%% where an .eps filename suffix will be assumed under latex,
%% and a .pdf suffix will be assumed for pdflatex; or what has been declared
%% via \DeclareGraphicsExtensions.
%\caption{Failure and repair of $\emph{FRU}_i$}
%\vspace{-0.25in}
%\label{failurerepair}
%\end{figure}
%
%From the field-gathered failure data, we can observe that the failure rates of
%different types of FRUs are non-constant, which means that the time to failure of FRUs in reality
%is not exponentially distributed. However, since the failure rates change slowly, they can still
%be treated as being constant within a relative short period time, such as 3 months. Our model will
%dynamically predict the failure rate of each FRU in the near future based on the
%historical failure logs.
%
%We use $\lambda_i$ and $\mu_i$ to denote the predicted failure rate and
%the repair rate of $\emph{FRU}_i$, where $\lambda_i \ll \mu_i$. As shown in
%Figure~\ref{failurerepair}, we define two random variables, $T_{i}^{u}$ and
%$T_{i}^{d}$ to represent the uptime and downtime of $\emph{FRU}_i$.  Since
%$T_{i}^{u}$ is actually the time to failure of $\emph{FRU}_i$, it is
%exponentially distributed, and its probability density function is
%$f_{T_{i}^{u}}(t) = \lambda_i e^{-\lambda_i t}$. If $\emph{FRU}_i$ has a spare
%part so that the replacement can start immediately after the failure occurs,
%$T_{i}^{d}$ is also exponentially distributed with a density function,
%$f_{T_{i}^{d}}(t) = \mu_i e^{-\mu_i t}$.  If $\emph{FRU}_i$ has no spare
%part, then after the failure occurs, there will be some delay before the replacement
%can start, $T_{i}^{d}$.
%This satisfies a shifted exponential distribution with density
%function $f_{T_{i}^{d}}(t) = \mu_i e^{-\mu_i (t-\tau_i)}$, where $\tau_i$ is the
%delay caused by waiting for a new $\emph{FRU}_i$ to be delivered to the field.
%
%Let us consider the scenario that $\emph{FRU}_i$ has a spare part first. Our goal
%is to calculate the probability that a specific time point $t$ is within the downtime of
%$\emph{FRU}_i$. Assume that at current time, $t_c$, no failure occurs, then as shown
%in Figure \ref{failurerepair}, $t$ is within the downtime of $\emph{FRU}_i$ only if
%$t_c-t_{i0} < T_{i}^{u} < t-t_{i0}$ and $T_{i}^{u}+T_{i}^{d} > t-t_{i0}$, where
%$t_{i0}$ is the time point when $\emph{FRU}_i$ recovered from the last failure.
%Therefore, the probability we need to calculate can be written as:
%\begin{equation}
%\begin{split}
%p_{i}^{s}(t) &= P(t_c-t_{i0} < T_{i}^{u} <
%t-t_{i0}, T_{i}^{u}+T_{i}^{d} > t-t_{i0}) \\
%&= P(T_{i}^{u}+T_{i}^{d} > t-t_{i0}
%\mid t_c-t_{i0} < T_{i}^{u} < t-t_{i0})\\
%&P(t_c-t_{i0} < T_{i}^{u} < t-t_{i0})\text{.}
%\end{split}
%\end{equation}
%In the above equation, $P(t_c-t_{i0} < T_{i}^{u} < t-t_{i0}) = \int_{t_c-t_{i0}}^{t-t_{i0}}\lambda_i e^{-\lambda_i x}dx$,
%as $T_{i}^{u}$ fits an exponential distribution.
%If we substitute $T_{i}^{u}$ with $x$, then $P(T_{i}^{u}+T_{i}^{d} > t-t_{i0})$
%can be written as:
%\begin{equation}
%\begin{split}
%P(T_{i}^{u}+T_{i}^{d} > t-t_{i0}) &=P(T_{i}^{d} > t-t_{i0}-x) \\
%&= \int_{t-t_{i0}-x}^{\infty}\mu_i e^{-\mu_i y}dy\\
%&=e^{-\mu_i (t-t_{i0}-x)}\text{.}
%\end{split}
%\end{equation}
%Therefore, the probability that $\emph{FRU}_i$ is down at time $t$ if it has a spare part
%can be calculated as:
%\begin{equation}
%\begin{split}
%p_{i}^{s}(t)&= \int_{t_c-t_{i0}}^{t-t_{i0}}e^{-\mu_i
%  (t-t_{i0}-x)}\lambda_i e^{-\lambda_i x}dx \\ &= \frac{\lambda_i e^{-\mu_i
%    (t-t_{i0})}}{\mu_i-\lambda_i}[e^{-(\lambda_i-\mu_i)(t-t_{i0})}-e^{-(\lambda_i-\mu_i)(t_c-t_{i0})}]\text{.}
%\end{split}
%\end{equation}
%
%Similarly, in the case that $\emph{FRU}_i$ has no spare part, we can also derive
%the probability that $\emph{FRU}_i$ is down at time $t$ by using probability
%density functions of $T_{i}^{u}$ and $T_{i}^{d}$:
%\begin{equation}
%\begin{split}
%p_{i}^{ns}(t)
%&= \int_{t_c-t_{i0}}^{t-t_{i0}}e^{-\mu_i (t-t_{i0}-x-\tau_i)}\lambda_i e^{-\lambda_i x}dx \text{.}
%\end{split}
%\end{equation}
%Since $\emph{FRU}_i$ has no spare part on-site, there is always a delay $\tau_i$
%caused by waiting for a new $\emph{FRU}_i$ to be delivered to the field.
%Therefore, $p_{i}^{ns}(t)$ is the summation of two probabilities, one is the probability that $t$ falls
%within the time interval when $\emph{FRU}_i$ is being replaced or repaired, the other
%is the probability that $t$ falls within the time interval when a new $\emph{FRU}_i$ is being
%awaited. Finally, $p_{i}^{ns}(t)$ is given as follows:
%\begin{equation}
%\begin{split}
%p_{i}^{ns}(t)&= \int_{t_c-t_{i0}}^{t-t_{i0}-\tau_i}e^{-\mu_i (t-t_{i0}-x-\tau_i)}\lambda_i e^{-\lambda_i x}dx\\
%&+\int_{t-t_{i0}-\tau_i}^{t-t_{i0}}\lambda_i e^{-\lambda_i x}dx \\
%&= \frac{\lambda_i e^{-\mu_i (t-t_{i0}-\tau_i)}}{\mu_i-\lambda_i}[e^{-(\lambda_i-\mu_i)(t-t_{i0}-\tau_i)}\\
%&-e^{-(\lambda_i-\mu_i)(t_c-t_{i0})}]+e^{-\lambda_i(t-t_{i0})}(e^{\lambda_i \tau_i}-1)\text{,}
%\end{split}
%\end{equation}
%and it is easy to verify that $p_{i}^{ns}(t)$ is always greater than $p_{i}^{s}(t)$.

\subsubsection{Impact of System Architecture on Availability}
To quantify the impact of the system architecture on data availability, we need
to analyze the physical structure of the system, and derive failure dependencies
between the different FRUs. Here we consider one SSU of Spider I
as an example. All FRUs of this SSU are listed in Table~\ref{frus}.

The RBD illustrates the failure dependencies between the different FRUs.
By analyzing the structure of the RBD, we can
derive the impact each FRU has on the data unavailability of the storage system.
For instance, each RAID group in the SSU
in Figure \ref{rbd} contains 10 disk drives, which are organized
as RAID level 6, and can tolerate 2 disk failures. Each leaf block represents a
disk drive, and there are 16 different paths from one leaf block to the root. On
each of these 16 paths, if one FRU fails, that path will be unavailable. If and
only if all of these 16 paths are unavailable, the associated disk drive will become
unavailable. If more than 2 disk drives in one RAID group are unavailable, a
data unavailability occurs. In fact, the more available paths each RAID group
has, the more reliable each RAID group is. Therefore, we can quantify the impact
of each FRU on data unavailability by counting the number of paths that will
become unavailable in one RAID group, if such an FRU has been removed from the RBD.

Specifically, since triple-disk unavailability in one RAID 6 group leads to
data unavailability, we only count unavailable paths of each triple-disk combination in
one RAID group. For example, failure of one controller makes every disk in one
RAID group lose 8 paths, while the failure of one disk enclosure only makes two
disks in one RAID group totally unavailable (each loses 16 paths). Therefore, we use
$8\times3=24$ as the impact of a controller, while $16\times2=32$ as that of a
disk enclosure. Table~\ref{fruimpact} shows the impact of each FRU quantified in this way.
%are listed in Table~\ref{fruimpact}.

\begin{table}[!h]
\footnotesize
\centering
\scalebox{0.7}{
%\begin{tabular}{|c||c||c|}
\begin{tabular}{|p{5.8cm}|p{0.5cm}|}
\multicolumn{1}{c}{} & \multicolumn{1}{c}{Quantified Impact}  \\
\cline{1-2}
Controller  &    24    \\
\hline
House Power Supply (Controller) &  12 \\
\hline
UPS Power Supply (Controller)& 12 \\
\hline
Disk Enclosure & 32 \\
\hline
House Power Supply (Disk Enclosure) & 16 \\
\hline
UPS Power Supply (Disk Enclosure)& 16 \\
\hline
I/O Module & 16 \\
\hline
Disk Expansion Module (DEM) & 8\\
\hline
Baseboard & 16\\
\hline
Disk Drive & 16 \\
\hline
\end{tabular}
}
\caption{Quantified impact of each type of FRU}
\vspace{-0.25in}
\label{fruimpact}
\end{table}

%\begin{table}[!h]
%\footnotesize
%\centering
%%\scalebox{0.8}{
%\begin{tabular}{|c||c|}
%\hline
%Name        & Method 1 & Method 2 \\
%\hline
%Controller  &    24   & 80  \\
%\hline
%Controller's Power Supply (House) & 12  & 40 \\
%\hline
%Controller's Power Supply (UPS)& 12 & 40 \\
%\hline
%Disk Enclosure & 32 & 32 \\
%\hline
%Disk Enclosure's Power Supply (House) & 16 & 16 \\
%\hline
%Disk Enclosure's Power Supply (UPS) & 16 & 16 \\
%\hline
%I/O Module & 16 & 16 \\
%\hline
%Disk Expansion Module (DEM) & 8 & 8 \\
%\hline
%Baseboard & 16 & 16 \\
%\hline
%Disk Drive & 16 & 16 \\
%\hline
%\end{tabular}
%%}
%\caption{Impact of each FRU}
%\vspace{-0.15in}
%\label{fruimpact}
%\end{table}

\subsubsection{Optimization Model and Dynamic Provisioning Algorithm}
In order to maximize data availability, our optimization model tries to
minimize the total unavailable time of the end-to-end paths that belong to each triple-disk combination
of a RAID group in the RBD.
%The intuition behind this is: the longer such end-to-end paths
%are unavailable, the more possible it is that the system encounters a data unavailability.
For example, as we mentioned above, one disk enclosure failure makes a triple-disk combination in
one RAID group lose 32 end-to-end paths. If the disk enclosure has no spare part on-site and cannot
be replaced quickly, those 32 end-to-end paths will be unavailable for a longer duration, which increases
the probability that all end-to-end paths of the triple-disk combination become unavailable within
the same time interval.

We define a variable $x_i$ to denote how many spare parts are provided for $\emph{FRU}_i$.
Then, the total unavailable time of the end-to-end paths, caused by failures of $\emph{FRU}_i$ can be
calculated as
\begin{equation}
\label{objfunc}
\begin{split}
\Delta t_{i}^{down} &=m_{i}x_{i}\emph{MTTR}_i+m_{i}(y_{i}-x_{i})(\emph{MTTR}_i+\tau_i) \text{,}
%& = m_{i}y_{i}(\emph{MTTR}_i+\tau_i)-m_{i}x_{i}\tau_i \text{,}
\end{split}
\end{equation}
where $m_{i}$ is the number of unavailable end-to-end paths caused by failures of $\emph{FRU}_i$
(see Table \ref{fruimpact}) and $y_{i}$ is the estimated number of $\emph{FRU}_i$ failures that would
occur before the next spare pool update.

%%%%%%%%%%%%%%%%% REMOVED

\begin{comment}
\newcommand{\rom}{\ensuremath{\mbox{\sc ResolveOptimizationModel}}}
\begin{algorithm}[t]
\algsetup{linenosize=\tiny}
\scriptsize
\caption{Spare Provisioning Algorithm}
\label{SPAlgorithm}
%\small
\begin{algorithmic}
\REQUIRE Current spare pool $\emph{SP}$, replacement log and unit price of each type of FRU, annual budget for spare provisioning $B$.
\ENSURE Spare provisioning results $\mathbf{X}=[x_1,x_2,\ldots,x_N]$.
\STATE Obtain number of spares in $\emph{SP}$, $\mathbf{n}=[n_1,n_2,\ldots,n_N]$;
\STATE Calculate $[m_1,m_2,\ldots,m_N]$;
\STATE Calculate $[\emph{MTTR}_1,\emph{MTTR}_2,\ldots,\emph{MTTR}_N]$
\FOR{$i=[1,2,\ldots,N]$}
    \STATE Calculate $y_{i}$, the expected number of failures of $\emph{FRU}_i$;
    \STATE Add $y_{i}$ into $\mathbf{Y}$, and $\emph{MTTR}_i$ into $\mathbf{MTTR}$;
    \STATE Add $m_i$ into $\mathbf{m}$, and $b_i$ into $\mathbf{b}$;
\ENDFOR
\STATE $\mathbf{X}=\rom(\mathbf{Y},\mathbf{MTTR},\mathbf{m},\mathbf{b},B)$
\FOR{$i=[1,2,\ldots,N]$}
    \IF{$n_i < x_i$}
        \STATE Add $(x_i-n_i)$ spares $\emph{FRU}_i$ in to $\emph{SP}$;
    \ENDIF
\ENDFOR
\end{algorithmic}
%\normalsize
\end{algorithm}
\end{comment}


Let us assume the unit price of $\emph{FRU}_i$ is $b_i$, the annual budget for
spare provisioning is $B$. Then, we can establish the following linear programming
optimization model to find out how many spares should be prepared for each type of FRU
in the coming year.
%optimize the spare pool for high data availability:
\begin{flalign}
\operatorname*{arg\,min}_{x_{i}}&\sum_{i=1}^{N} m_{i}y_{i}(\emph{MTTR}_i+\tau_i)-m_{i}x_{i}\tau_i\text{;} \\
\text{s.t.} &\sum_{i=1}^{N}x_{i}b_{i}\leq B \text{;}\\
            & x_i \leq y_i, \forall i \in \{1\dots N\}
\end{flalign}
In this linear programming model, the objective function is to minimize the total
unavailable time of the end-to-end paths that belong to each triple-disk combination
of a RAID group in the RBD. The two constraints are that the total provisioning cost
cannot exceed the annual budget and for each type of FRU, the number of
provisioned spares should not exceed the expected number of failures.

%The pseudo-code of the spare provisioning algorithm can be found at \cite{SPA}.
The pseudo-code of the spare provisioning algorithm is shown in Algorithm~\ref{SPAlgorithm}.
At the beginning of each of year, system administrators
can first check the spare pool, and find out which FRU has no spare part. Then they
can calculate all required parameters for these unprovisioned FRUs,
and resolve the optimization model to find out
those that need a spare part. Finally, based on the optimization results, they add the needed
spare parts into the spare pool.
\newcommand{\rom}{\ensuremath{\mbox{\sc ResolveOptimizationModel}}}
\begin{algorithm}[t]
\algsetup{linenosize=\tiny}
\scriptsize
\caption{Spare Provisioning Algorithm}
\label{SPAlgorithm}
%\small
\begin{algorithmic}
\REQUIRE Current spare pool $\emph{SP}$, replacement log and unit price of each type of FRU, annual budget for spare provisioning $B$.
\ENSURE Spare provisioning results $\mathbf{X}=[x_1,x_2,\ldots,x_N]$.
\STATE Obtain number of spares in $\emph{SP}$, $\mathbf{n}=[n_1,n_2,\ldots,n_N]$;
\STATE Calculate $[m_1,m_2,\ldots,m_N]$;
\STATE Calculate $[\emph{MTTR}_1,\emph{MTTR}_2,\ldots,\emph{MTTR}_N]$
\FOR{$i=[1,2,\ldots,N]$}
    \STATE Calculate $y_{i}$, the expected number of failures of $\emph{FRU}_i$;
    \STATE Add $y_{i}$ into $\mathbf{Y}$, and $\emph{MTTR}_i$ into $\mathbf{MTTR}$;
    \STATE Add $m_i$ into $\mathbf{m}$, and $b_i$ into $\mathbf{b}$;
\ENDFOR
\STATE $\mathbf{X}=\rom(\mathbf{Y},\mathbf{MTTR},\mathbf{m},\mathbf{b},B)$
\FOR{$i=[1,2,\ldots,N]$}
    \IF{$n_i < x_i$}
        \STATE Add $(x_i-n_i)$ spares $\emph{FRU}_i$ in to $\emph{SP}$;
    \ENDIF
\ENDFOR
\end{algorithmic}
%\normalsize
\end{algorithm}

\begin{comment}
We define the following variable to indicate whether $\emph{FRU}_i$
should have a spare part or not:
\begin{equation}
x_{i} = \begin{dcases*}
    1 & $\emph{FRU}_i$ has spare part;\\
    0 & otherwise.
    \end{dcases*}
\end{equation}
As we have already derived $p_{i}^{s}(t)$ and $p_{i}^{ns}(t)$, we can obtain the
expected number of unavailable paths using the numbers calculated by the two
methods we mentioned above:
$m_i\{x_i[p_{i}^{s}(t)-p_{i}^{ns}(t)]+p_{i}^{ns}(t)\}$, where $m_i$ is the
impact $\emph{FRU}_i$ has on data unavailability, which can be found in Table~\ref{fruimpact}.

Assume the unit price of $\emph{FRU}_i$ is $b_i$, and the annual budget for
spare provisioning is $B$.  $\emph{NS}$ is the set of FRUs which have no spare
part in the spare pool.  At the beginning of every year, we resolve the following
linear programming model to optimize the spare pool for high data availability:
\begin{flalign}
\operatorname*{arg\,min}_{x_{i}}&\sum_{i \in \emph{NS}}m_i\{x_i[p_{i}^{s}(t)-p_{i}^{ns}(t)]+p_{i}^{ns}(t)\} \text{;} \\
\text{s.t.} &\sum_{i \in \emph{NS}}x_{i}b_{i}\leq B \text{.}
\end{flalign}
The objective function of the linear programming model is to minimize the expected
number of unavailable paths caused by all failed FRUs, while the constraint is that
the provisioning cost cannot exceed the annual budget.
The pseudo-code implementation of the spare provisioning algorithm can be
found at~\cite{SPA}.  At the beginning of each of year, system administrators
can first check the spare pool, and find out which FRU has no spare part. Then they
can calculate all required parameters for these unprovisioned FRUs,
and resolve the optimization model to find out
those that need a spare part. Finally, based on the optimization results, they add the needed
spare parts into the spare pool. Note that in order to save the budget,
our provisioning algorithm tries not to
over-provision the spare parts, which means at any time, for each FRU, there is at most one
spare part in the spare pool.

\newcommand{\rom}{\ensuremath{\mbox{\sc ResolveOptimizationModel}}}

\begin{algorithm}[t]
\caption{Spare Provisioning Algorithm}
\label{SPAlgorithm}
\small
\begin{algorithmic}
\REQUIRE Current spare pool $\emph{SP}$, replacement log and unit price of each FRU, annual budget for spare provisioning.
\ENSURE Spare provisioning results $\mathbf{X}=[x_1,x_2,\ldots,x_N]$.
\STATE Calculate $[m_1,m_2,\ldots,m_N]$;
\STATE Initialize $\mathbf{X}=[0,0,\ldots,0]$
\FOR{$i=[1,2,\ldots,N]$}
    \IF{$\emph{FRU}_i$ has no spare part in \emph{SP}}
        \STATE Add $i$ into $\emph{NS}$;
    \ENDIF
\ENDFOR
\FOR{$j$ in $\emph{NS}$}
    \STATE Predict $\lambda_{i}$ and $\mu_{i}$;
    \STATE Calculate $p_{j}^{s}(t)$ and $p_{j}^{ns}(t)$;
    \STATE Add $p_{j}^{s}(t)$ into $\mathbf{P^{s}}$, add $p_{j}^{ns}(t)$ into $\mathbf{P^{ns}}$;
    \STATE Add $m_j$ into $\mathbf{m}$, add $b_j$ into $\mathbf{b}$;
\ENDFOR
\STATE $\mathbf{X}=\rom(\mathbf{P^{s}},\mathbf{P^{ns}},\mathbf{m},\mathbf{b},B)$
\FOR{$i=[1,2,\ldots,N]$}
    \IF{$x_i==1$}
        \STATE Add a spare $\emph{FRU}_i$ in to $\emph{SP}$;
    \ENDIF
\ENDFOR
\end{algorithmic}
\normalsize
\end{algorithm}


\end{comment}
